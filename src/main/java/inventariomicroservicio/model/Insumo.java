package inventariomicroservicio.model;

import java.sql.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Insumo {
    private long insumoID;
    private int cantidad;
    private String fechaFabricacion;
    private String fechaExpiracion;
    private boolean esCaducado = false;
    private long tipoInsumoID;


    public Insumo(long insumoID, int cantidad, String fechaFabricacion, String fechaExpiracion, boolean esCaducado, long tipoInsumoID) {
        this.insumoID = insumoID;
        this.cantidad = cantidad;
        this.fechaFabricacion = fechaFabricacion;
        this.fechaExpiracion = fechaExpiracion;
        this.esCaducado = esCaducado;
        this.tipoInsumoID = tipoInsumoID;
    }

    public Insumo() {
    }


    public long getInsumoID() {
        return this.insumoID;
    }

    public void setInsumoID(long insumoID) {
        this.insumoID = insumoID;
    }

    public int getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getFechaFabricacion() {
        return this.fechaFabricacion;
    }

    public void setFechaFabricacion(String fechaFabricacion) {
        this.fechaFabricacion = fechaFabricacion;
    }

    public String getFechaExpiracion() {
        return this.fechaExpiracion;
    }

    public void setFechaExpiracion(String fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public boolean isEsCaducado() {
        return this.esCaducado;
    }

    public boolean getEsCaducado() {
        return this.esCaducado;
    }

    public void setEsCaducado(boolean esCaducado) {
        this.esCaducado = esCaducado;
    }

    public long getTipoInsumoID() {
        return this.tipoInsumoID;
    }

    public void setTipoInsumoID(long tipoInsumoID) {
        this.tipoInsumoID = tipoInsumoID;
    }

}
