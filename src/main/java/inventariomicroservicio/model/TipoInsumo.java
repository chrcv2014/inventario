package inventariomicroservicio.model;

public class TipoInsumo {

    private long tipoInsumoID;
    private String nombre;
    private String descripcion;


    public TipoInsumo() {
    }

    public TipoInsumo(long tipoInsumoID, String nombre, String descripcion) {
        this.tipoInsumoID = tipoInsumoID;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public long getTipoInsumoID() {
        return this.tipoInsumoID;
    }

    public void setTipoInsumoID(long tipoInsumoID) {
        this.tipoInsumoID = tipoInsumoID;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
