package inventariomicroservicio.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import inventariomicroservicio.model.TipoInsumo;

public interface TipoInsumoRepositoryInterface extends BaseRepository<TipoInsumo>{
        public boolean create(TipoInsumo tipoInsumo);
    public boolean update(TipoInsumo tipoInsumo);

    public List<TipoInsumo> read(Pageable pageable);
}
