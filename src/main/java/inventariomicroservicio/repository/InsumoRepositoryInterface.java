package inventariomicroservicio.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;

import inventariomicroservicio.model.Insumo;

public interface InsumoRepositoryInterface extends BaseRepository<Insumo> {
    public boolean create(Insumo insumo);
    public boolean update(Insumo insumo);
    public boolean hasExpired(Insumo insumo);

    public List<Insumo> read(Pageable pageable);
}
