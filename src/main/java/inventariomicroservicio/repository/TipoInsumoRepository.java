package inventariomicroservicio.repository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import inventariomicroservicio.mappers.TipoInsumoMapper;
import inventariomicroservicio.model.TipoInsumo;

@Repository
public class TipoInsumoRepository implements TipoInsumoRepositoryInterface {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    private static final Logger logger = LoggerFactory.getLogger(TipoInsumo.class);

    @Override
    public boolean create(TipoInsumo tipoInsumo) {
        try{
            String sql = String.format(
            "insert into tipo_insumos(nombre,descripcion) values('%s','%s')" 
            , tipoInsumo.getNombre(), tipoInsumo.getDescripcion());
            jdbcTemplate.execute(sql);
            return true;}
            catch(Exception e){
                return false;
            } 
    }

@Override
public boolean update(TipoInsumo tipoInsumo) {
    if (tipoInsumo.getTipoInsumoID() > 0) {
        String sql = "UPDATE tipo_insumos SET nombre=?, descripcion=? WHERE tipoInsumoID=?";
        Object[] params = new Object[]{tipoInsumo.getNombre(), tipoInsumo.getDescripcion(), tipoInsumo.getTipoInsumoID()};

        try {
            jdbcTemplate.update(sql, params);
            return true;
        } catch (DataAccessException e) {
            logger.error("Error updating tipoInsumo.", e);
            return false;
        }
    }
    return false;
}

    @Override
    public List<TipoInsumo> read(Pageable pageable) {
        return jdbcTemplate.query("select * from inventario_microservicio.tipo_insumos", new TipoInsumoMapper());
    }

    @Override
    public TipoInsumo readById(int Id) {
        Object[] params = new Object[] {Id};
        return jdbcTemplate.queryForObject("select * from tipo_insumos where tipoInsumoID = ?",  params, new TipoInsumoMapper());
    }
}
