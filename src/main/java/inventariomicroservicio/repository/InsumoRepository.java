package inventariomicroservicio.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import inventariomicroservicio.mappers.InsumoMapper;
import inventariomicroservicio.model.Insumo;
import inventariomicroservicio.model.TipoInsumo;

@Repository
public class InsumoRepository implements InsumoRepositoryInterface{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(Insumo.class);

    @Override
    public boolean create(Insumo insumo) {
        try {
            String sql = "INSERT INTO insumos (cantidad, fechaFabricacion, fechaExpiracion, tipoInsumoID) VALUES (?, ?, ?, ?)";

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            java.sql.Date fechaFabricacion = new java.sql.Date(dateFormat.parse(insumo.getFechaFabricacion()).getTime());
            java.sql.Date fechaExpiracion = new java.sql.Date(dateFormat.parse(insumo.getFechaExpiracion()).getTime());

            jdbcTemplate.update(sql, insumo.getCantidad(), fechaFabricacion, fechaExpiracion, insumo.getTipoInsumoID());
            return true;
        }
        catch(Exception e){
            logger.error("An exception occurred during create method execution.", e);

            return false;
        }    
    }

    @Override
    public boolean update(Insumo insumo) {
        if(insumo.getInsumoID() > 0){

            String sql = "UPDATE insumo SET cantidad=?, fechaFabricacion=?, fechaExpirado=?, tipoInsumoID=? where insumoID=?";
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try{
            java.sql.Date fechaFabricacion = new java.sql.Date(dateFormat.parse(insumo.getFechaFabricacion()).getTime());
            java.sql.Date fechaExpiracion = new java.sql.Date(dateFormat.parse(insumo.getFechaExpiracion()).getTime());

            Object [] params = new Object[] {insumo.getCantidad(), fechaFabricacion, fechaExpiracion, insumo.getTipoInsumoID(), insumo.getInsumoID()};
            jdbcTemplate.update(sql, params);
            return true;}
         catch (ParseException e) {
            logger.error("Error parsing date.", e);
            return false;
        } catch (DataAccessException e) {
            logger.error("Error updating insumo.", e);
            return false;
        }   
        }return false;
     
    }



    @Override
    public List<Insumo> read(Pageable pageable) {
      return jdbcTemplate.query("select * from insumos", new InsumoMapper());
    }

    @Override
    public Insumo readById(int Id) {
            Object[] params = new Object[] {Id};
        return jdbcTemplate.queryForObject("select * from insumos where tipoInsumoID = ?",  params, new InsumoMapper());
    }

    @Override
    public boolean hasExpired(Insumo insumo) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'hasExpired'");
    }

    
}
