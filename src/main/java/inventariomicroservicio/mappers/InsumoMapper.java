package inventariomicroservicio.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import inventariomicroservicio.model.Insumo;

public class InsumoMapper implements RowMapper<Insumo> {

    @Override
    @Nullable
    public Insumo mapRow(ResultSet rs, int rowNum) throws SQLException {
        Insumo insumo = new Insumo();

        insumo.setInsumoID(rs.getLong("insumoID"));
        insumo.setCantidad(rs.getInt("cantidad"));
        insumo.setFechaFabricacion(rs.getString("fechaFabricacion"));
        insumo.setFechaExpiracion(rs.getString("fechaExpiracion"));
        insumo.setEsCaducado(rs.getBoolean("esCaducado"));
        insumo.setTipoInsumoID(rs.getLong("tipoInsumoID"));

        return insumo;

    }
    
}
