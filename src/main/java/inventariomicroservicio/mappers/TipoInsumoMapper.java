package inventariomicroservicio.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import inventariomicroservicio.model.TipoInsumo;


public class TipoInsumoMapper implements RowMapper<TipoInsumo> {

    @Override
    public TipoInsumo mapRow(ResultSet rs, int rowNum) throws SQLException {
   
        TipoInsumo tipoInsumo = new TipoInsumo();
        tipoInsumo.setTipoInsumoID(rs.getLong("tipoInsumoID"));
        tipoInsumo.setNombre(rs.getString("nombre"));
        tipoInsumo.setDescripcion(rs.getString("descripcion"));
        return tipoInsumo;
    }
    
}
