package inventariomicroservicio;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;


@SpringBootApplication
public class InventarioMicroservicioApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(InventarioMicroservicioApplication.class, args);
	}


	
	@Autowired
	private JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... args) throws Exception {
        Path path = Paths.get("src/main/resources/import.sql");
        Log log = LogFactory.getLog(getClass());

        try (BufferedReader bufferedReader = Files.newBufferedReader(path, Charset.forName("UTF-8"))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                jdbcTemplate.execute(line);
            }
        } catch (IOException ex) {
            log.error("Error reading import.sql file: " + ex.getMessage());
        }
    }

}
