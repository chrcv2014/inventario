package inventariomicroservicio.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ch.qos.logback.core.model.Model;
import inventariomicroservicio.model.TipoInsumo;
import inventariomicroservicio.repository.TipoInsumoRepository;

@Controller
@RequestMapping("/tipoinsumos")
public class TipoInsumoController {

    @Autowired
    private TipoInsumoRepository tipoInsumoRepository;

    @GetMapping(path = "")
    public ModelAndView home() {

        ModelAndView modelAndView = new ModelAndView("indexTipoInsumo.html");

        return modelAndView;
    }

    @GetMapping(path = "/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView("createTipoInsumo.html");
        modelAndView.addObject("tipoInsumo", new TipoInsumo());
        return modelAndView;
    }

    @PostMapping(path = "/create")
    public String created(@ModelAttribute TipoInsumo tipoInsumo) {
        tipoInsumoRepository.create(tipoInsumo);

        return "redirect:/tipoinsumos/create";
    }

    @GetMapping(path = "/read")
    public ModelAndView read(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("readTipoInsumo.html");
        modelAndView.addObject("tipoInsumos", tipoInsumoRepository.read(pageable));
        return modelAndView;
    }

    @GetMapping(path = "/search")
    public ModelAndView search() {
        ModelAndView modelAndView = new ModelAndView("searchTipoInsumo.html");
        modelAndView.addObject("tipoInsumo", new TipoInsumo());
        return modelAndView;
    }

    @PostMapping(path = "/search")
    public String searched(@ModelAttribute TipoInsumo tipoInsumo) {
        int tipoInsumoID = (int) tipoInsumo.getTipoInsumoID();        
        return "redirect:/tipoinsumos/update?tipoInsumoID=" + tipoInsumoID;
    }
    
    @GetMapping(path = "/update")
    public ModelAndView update(@RequestParam("tipoInsumoID") int tipoInsumoID) {
        ModelAndView modelAndView = new ModelAndView("updateTipoInsumo.html");
        modelAndView.addObject("tipoInsumo", tipoInsumoRepository.readById(tipoInsumoID));
        return modelAndView;
    }

    @PostMapping(path = "/update")
    public String updated(@ModelAttribute TipoInsumo tipoInsumo) {
        if (tipoInsumo.getTipoInsumoID() > 0) {
            tipoInsumoRepository.update(tipoInsumo);

        } else {
            tipoInsumoRepository.update(tipoInsumo);
        }
        return "redirect:/tipoinsumos";
    }

}
