package inventariomicroservicio.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import inventariomicroservicio.model.Insumo;
import inventariomicroservicio.model.TipoInsumo;
import inventariomicroservicio.repository.InsumoRepository;

@Controller
@RequestMapping("/insumos")
public class InsumoController {

    @Autowired
    InsumoRepository insumoRepository;

    @GetMapping(path = "")
    public String home() {
        return "indexInsumo.html";
    }
    @GetMapping(path = "/create")
    public ModelAndView create() {

        ModelAndView modelAndView = new ModelAndView("createInsumo.html");
        modelAndView.addObject("insumo", new Insumo());
        return modelAndView;
    }

    @PostMapping(path = "/create")
    public String created(@ModelAttribute Insumo insumo) {
        insumoRepository.create(insumo);

        return "redirect:/insumos/create";
    }
    
    @GetMapping(path = "/read")
    public ModelAndView read(Pageable pageable) {
        ModelAndView modelAndView = new ModelAndView("readInsumo.html");
        modelAndView.addObject("insumo", insumoRepository.read(pageable));
        return modelAndView;
    }

      @GetMapping(path = "/search")
    public ModelAndView search() {
        ModelAndView modelAndView = new ModelAndView("searchInsumo.html");
        modelAndView.addObject("insumo", new Insumo());
        return modelAndView;
    }

    @PostMapping(path = "/search")
    public String searched(@ModelAttribute Insumo insumo) {
        int insumoID = (int) insumo.getInsumoID();        
        return "redirect:/insumos/update?insumoID=" + insumoID;
    }
    
    @GetMapping(path = "/update")
    public ModelAndView update(@RequestParam("insumoID") int insumoID) {
        ModelAndView modelAndView = new ModelAndView("updateInsumo.html");
        modelAndView.addObject("insumo", insumoRepository.readById(insumoID));
        return modelAndView;
    }

    @PostMapping(path = "/update")
    public String updated(@ModelAttribute Insumo insumo) {
        if (insumo.getTipoInsumoID() > 0) {
            insumoRepository.update(insumo);

        } else {
            insumoRepository.update(insumo);
        }
        return "redirect:/insumos";
    }



}
